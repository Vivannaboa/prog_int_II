﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarefa1_Cadastro.Entities;

namespace Tarefa1_Cadastro
{
    class Program
    {
        public static List<Pessoa> _listPessoas = new List<Pessoa>();

        static void Main(string[] args)
        {

            int opcao;
            var scape = "";
            do
            {
                Console.WriteLine("Bem vindo ao sistema de cadastro de pessoa!");
                Console.WriteLine("Opções:");
                Console.WriteLine("1 - Adicionar.");
                Console.WriteLine("2 - Alterar.");
                Console.WriteLine("3 - Excluir.");
                Console.WriteLine("4 - Listar.");
                Console.WriteLine("5 - Sair.");
                Console.WriteLine("-------------------------------------");
                Console.Write("Digite uma opção: ");
                scape = Console.ReadLine();
                if (!scape.Equals("") && scape.All(char.IsDigit))
                {
                    opcao = Int32.Parse(scape);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Opção inválida!");
                    opcao = 30;
                }

                switch (opcao)
                {
                    case 1:
                        cadastrar();
                        break;
                    case 2:
                        editar();
                        break;
                    case 3:
                        excluir();
                        break;
                    case 4:
                        listar();
                        break;
                    case 5:
                        saiPrograma();
                        opcao = 0;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Opção inválida!");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            } while (opcao != 0);
        }
        private static void saiPrograma()
        {
            Console.WriteLine();
            Console.WriteLine("Você saiu do Programa. Clique qq tecla para sair...");
        }

        private static void cadastrar()
        {
            Console.Clear();
            //intanciar pessoa
            Pessoa p1 = new Pessoa();
            p1.DataCadastro = DateTime.Now;

            Console.WriteLine("******************| Cadastro de Pessoas |***********************");
            Console.WriteLine();

            Console.WriteLine("Informe o nome");
            p1.Nome = Console.ReadLine();
            Console.WriteLine("Informe o FPF");
            p1.Cpf = Console.ReadLine();
            Console.WriteLine("Informe o RG");
            p1.Rg = Console.ReadLine();
            Console.WriteLine("Informe o E-mail");
            p1.Email = Console.ReadLine();
            Console.WriteLine("Informe o endereço");
            p1.Endereco = Console.ReadLine();
            Console.WriteLine("Informe o número");
            p1.Numero = Console.ReadLine();
            Console.WriteLine("Informe o bairro");
            p1.Bairro = Console.ReadLine();
            Console.WriteLine("Informe a cidade");
            p1.Cidade = Console.ReadLine();
            _listPessoas.Add(p1);
            Console.WriteLine("Cadastro efetuado com sucesso! Clique para voltar.");
        }

        private static void listar()
        {
            Console.WriteLine("******************| Pessoas Cadastradas |***********************");
            var i = 0;
            foreach (Pessoa item in _listPessoas)
            {
                i++;
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("Pessoa " + i);
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("id: " + item.Id);
                Console.WriteLine("Nome: " + item.Nome);
                Console.WriteLine("CPF: " + item.Cpf);
                Console.WriteLine("RG: " + item.Rg);
                Console.WriteLine("Data Cadastro: " + item.DataCadastro);
                Console.WriteLine("Data Modificacão: " + item.DataModificacao);
                Console.WriteLine("E-mail: " + item.Email);
                Console.WriteLine("Endereço: " + item.Endereco);
                Console.WriteLine("Número: " + item.Numero);
                Console.WriteLine("Bairro: " + item.Bairro);
                Console.WriteLine("Cidade: " + item.Cidade);

            }
        }
        private static void editar()
        {
            Pessoa p1 = null;
            string nome;
            Console.WriteLine("Informe o nome do cliente que deseja alterar!");
            nome = Console.ReadLine();
            foreach (Pessoa item in _listPessoas)
            {
                if (item.Nome.Equals(nome))
                {
                    p1 = item;
                }
            }
            if (p1 != null)
            {
                Console.WriteLine("******************| Alteração de cadastro de Pessoas |***********************");
                Console.WriteLine();

                p1.DataModificacao = DateTime.Now;
                Console.WriteLine("Informe o nome");
                p1.Nome = Console.ReadLine();
                Console.WriteLine("Informe o FPF");
                p1.Cpf = Console.ReadLine();
                Console.WriteLine("Informe o RG");
                p1.Rg = Console.ReadLine();
                Console.WriteLine("Informe o E-mail");
                p1.Email = Console.ReadLine();
                Console.WriteLine("Informe o endereço");
                p1.Endereco = Console.ReadLine();
                Console.WriteLine("Informe o número");
                p1.Numero = Console.ReadLine();
                Console.WriteLine("Informe o bairro");
                p1.Bairro = Console.ReadLine();
                Console.WriteLine("Informe a cidade");
                p1.Cidade = Console.ReadLine();

                Console.WriteLine("Registro Alterado com sucesso!");
            }
            else
            {
                Console.WriteLine("Ops, não conseguimos encontrar esse cadastro!");
            }
        }
        private static void excluir()
        {
            Pessoa p1 = null;
            string nome;
            Console.WriteLine("Informe o nome do cliente que deseja alterar!");
            nome = Console.ReadLine();
            foreach (Pessoa item in _listPessoas)
            {
                if (item.Nome.Equals(nome))
                {
                    p1 = item;
                }
            }
            if (p1 != null)
            {
                _listPessoas.Remove(p1);
            }
            else
            {
                Console.WriteLine("Ops, não conseguimos encontrar esse cadastro!");
            }
        }
    }
}
